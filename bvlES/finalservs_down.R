library(raster)

ABO_P_AbMat_NonMetallic<-raster('down_ABO_P_AbMat_NonMetallic.tif')
ABO_P_AbMat_Water<-raster('down_ABO_P_AbMat_Water.tif')
ABO_P_NutAbSubst_Water<-raster('down_ABO_P_NutAbSubst_Water.tif')
ABO_RM_MedFlo_BySolidLiquidGaseousFlows<-raster('down_ABO_RM_MedFlo_BySolidLiquidGaseousFlows.tif')
ABO_RM_MedWast_ByNaturalChemicalPhysicalProcesses<-raster('down_ABO_RM_MedWast_ByNaturalChemicalPhysicalProcesses.tif')
ESS_C_PhysIntel_IntellectualRepresentativeInteractions<-raster('down_ESS_C_PhysIntel_IntellectualRepresentativeInteractions.tif')
ESS_C_PhysIntel_PhysicalExperientialInteractions<-raster('down_ESS_C_PhysIntel_PhysicalExperientialInteractions.tif')
ESS_C_SpiritSymb_OtherCulturalOutputs<-raster('down_ESS_C_SpiritSymb_OtherCulturalOutputs.tif')
ESS_P_En_MechanicalEnergy<-raster('down_ESS_P_En_MechanicalEnergy.tif')
ESS_P_Mat_Biomass<-raster('down_ESS_P_Mat_Biomass.tif')
ESS_P_Nut_Biomass<-raster('down_ESS_P_Nut_Biomass.tif')
ESS_RM_MaintPhChBioCond_AtmosphericCompositionClimateRegulation<-raster('down_ESS_RM_MaintPhChBioCond_AtmosphericCompositionClimateRegulation.tif')
ESS_RM_MaintPhChBioCond_LifecycleMaintHabitatGenePoolProtection<-raster('down_ESS_RM_MaintPhChBioCond_LifecycleMaintHabitatGenePoolProtection.tif')
ESS_RM_MaintPhChBioCond_PestDiseaseControl<-raster('down_ESS_RM_MaintPhChBioCond_PestDiseaseControl.tif')
ESS_RM_MaintPhChBioCond_SoilFormationComposition<-raster('down_ESS_RM_MaintPhChBioCond_SoilFormationComposition.tif')
ESS_RM_MaintPhChBioCond_WaterConditions<-raster('down_ESS_RM_MaintPhChBioCond_WaterConditions.tif')
ESS_RM_MedFlo_GaseousAirFlows<-raster('down_ESS_RM_MedFlo_GaseousAirFlows.tif')
ESS_RM_MedFlo_LiquidFlows<-raster('down_ESS_RM_MedFlo_LiquidFlows.tif')
ESS_RM_MedFlo_MassFlows<-raster('down_ESS_RM_MedFlo_MassFlows.tif')
ESS_RM_MedWast_MediationBiota<-raster('down_ESS_RM_MedWast_MediationBiota.tif')
ESS_RM_MedWast_MediationEcosystems<-raster('down_ESS_RM_MedWast_MediationEcosystems.tif')
ABO_C_PhysIntel_IntellectualRepresentativeInteractions<-raster('down_ABO_C_PhysIntel_IntellectualRepresentativeInteractions.tif')
ABO_C_PhysIntel_PhysicalExperientialInteractions<-raster('down_ABO_C_PhysIntel_PhysicalExperientialInteractions.tif')

################### services

s1<-ESS_P_En_MechanicalEnergy

# there is no s2

s3<-ESS_P_Mat_Biomass

s4<-ABO_P_AbMat_NonMetallic + ABO_P_AbMat_Water


s5<-ESS_P_Nut_Biomass

s6<-ABO_P_NutAbSubst_Water

s7<-ABO_RM_MedFlo_BySolidLiquidGaseousFlows + ESS_RM_MedFlo_GaseousAirFlows + ESS_RM_MedFlo_LiquidFlows + ESS_RM_MedFlo_MassFlows


s8<-ABO_RM_MedWast_ByNaturalChemicalPhysicalProcesses + ESS_RM_MedWast_MediationBiota + ESS_RM_MedWast_MediationEcosystems

s9<-ESS_RM_MaintPhChBioCond_AtmosphericCompositionClimateRegulation + ESS_RM_MaintPhChBioCond_LifecycleMaintHabitatGenePoolProtection + ESS_RM_MaintPhChBioCond_PestDiseaseControl + ESS_RM_MaintPhChBioCond_SoilFormationComposition + ESS_RM_MaintPhChBioCond_WaterConditions

s10<-ABO_C_PhysIntel_IntellectualRepresentativeInteractions + ABO_C_PhysIntel_PhysicalExperientialInteractions + ESS_C_PhysIntel_IntellectualRepresentativeInteractions + ESS_C_PhysIntel_PhysicalExperientialInteractions

s11<-ESS_C_SpiritSymb_OtherCulturalOutputs

writeRaster(s1,'down_s1.tif',overwrite=T)
writeRaster(s3,'down_s3.tif',overwrite=T)
writeRaster(s4,'down_s4.tif',overwrite=T)
writeRaster(s5,'down_s5.tif',overwrite=T)
writeRaster(s6,'down_s6.tif',overwrite=T)
writeRaster(s7,'down_s7.tif',overwrite=T)
writeRaster(s8,'down_s8.tif',overwrite=T)
writeRaster(s9,'down_s9.tif',overwrite=T)
writeRaster(s10,'down_s10.tif',overwrite=T)
writeRaster(s11,'down_s11.tif',overwrite=T)


