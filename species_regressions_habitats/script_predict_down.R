library(raster)
library(rasterVis)

sal<-raster('salmod2_ll.tif')
elev<-raster('elevation.tif')
#dist<-raster('effdist.tif')
#dist2<-reclassify(dist,c(100,Inf,100))

#elev2<-resample(elev,dist)
sal2<-resample(sal,elev)
sal3<-mask(sal2,elev)

tacl<-elev
tacl[!is.na(tacl)]<-55
s<-stack(sal3,elev,tacl)
names(s)<-c('sal_mean','elev','tacl')

ss<-s[[1:2]]

  nl <- nlayers(ss)
  m <- matrix( 1:nl, nrow=1)
  themes <- list(plasmaTheme(), plasmaTheme())
  names<-list('Mean salinity (PSU)','Elevation (m)')

tiff('input_variables_down.tif',width=1152,height=864,res=200)
#png('input_variables_down.png',width=1152,height=864,res=200)

  for (i in 1:nl){
      p <- levelplot(ss, layers=i,
		#scales=list(draw=FALSE),
		par.settings=themes[[i]],
                margin=FALSE,
		xlab=NULL, ylab=NULL
		,main=names[[i]]
		) 

      print(p, split=c(col(m)[i], row(m)[i], ncol(m), nrow(m)), more=(i<nl))
  }

dev.off()


##mod_bol_d_br<-predict(s,opt_bol_d_br@objects[[1]],type='response')

#mod_bol_d_log<-invlog(predict(s,opt_bol_d_log@objects[[1]],type='response'))

#mod_bol_d_rf<-predict(s,rfbol,type='response')

#mod_jun_d_rf<-predict(s,rfjun,type='response')

#mod_hal_d_rf<-predict(s,rfhal,type='response')

#mod_mud_d_rf<-predict(s,rfmud,type='response')

#mod_ph_d_rf<-predict(s,rfph,type='response')

#mod_bol_d_qb<-predict(s,bol_d_qb,type='response')

#mod_hal_d_br<-predict(s,opt_hal_d_br@objects[[1]],type='response')

#mod_jun_d_br<-predict(s,opt_jun_d_br@objects[[1]],type='response')

#mod_mud_d_br<-predict(s,opt_mud_d_br@objects[[1]],type='response')

#mod_ph_d_br<-predict(s,opt_ph_d_br@objects[[1]],type='response')

mod_ph_down_br<-predict(s,opt_ph_down_br@objects[[1]],type='response')

mod_bol_down_br<-predict(s,opt_bol_down_br@objects[[1]],type='response')

mod_hal_down_br<-predict(s,opt_hal_down_br@objects[[1]],type='response')

mod_jun_down_br<-predict(s,opt_jun_down_br@objects[[1]],type='response')

mod_mud_down_br<-predict(s,opt_mud_down_br@objects[[1]],type='response')

sp<-stack(mod_bol_down_br,mod_jun_down_br,mod_hal_down_br,mod_ph_down_br,mod_mud_down_br)
names(sp)<-c('Bolboschoenus','Juncus','Halimione','Phragmites','Mud')


tiff('species_models_down_betareg_2015.tif',width=1680,height=1050, res=200)
#png('species_models_down_betareg_2015.png',width=1680,height=1050, res=200)

levelplot(sp,par.settings=PuOrTheme(),names.attr=c(expression(italic('B. maritimus')),expression(italic('J. maritimus')),expression(italic('H. portulacoides')),expression(italic('P. australis')),'Mud flats'))
	#,par.strip.text=list(font=c(3))) 

dev.off()


