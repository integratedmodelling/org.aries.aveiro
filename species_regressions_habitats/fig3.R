library(gridExtra)
library(raster)
library(rasterVis)

# down

ph_down<-raster('phragmites_down.tif')
bol_down<-raster('bolboschoenus_down.tif')
jun_down<-raster('juncus_down.tif')
hal_down<-raster('halimione_down.tif')
mud_down<-raster('mud_down.tif')

scale = list("SpatialPolygonsRescale", layout.scale.bar(), 
    offset = c(-8.63,40.67), scale = 0.044988708, fill=c("transparent","black"))
text = list("sp.text", c(-8.61,40.68), "5 km")

arrow = list("SpatialPolygonsRescale", layout.north.arrow(), 
    offset = c(-8.66,40.73), scale = 0.02)


p_bol_down<-levelplot(bol_down,col.regions=sort(terrain.colors(100),decreasing=T),main=expression(italic('B. maritimus')),margin=FALSE,at=seq(0,1,0.1),scales=list(draw=FALSE),xlab=NULL, ylab=NULL)

p_jun_down<-levelplot(jun_down,col.regions=sort(terrain.colors(100),decreasing=T),main=expression(italic('J. maritimus')),margin=FALSE,at=seq(0,1,0.1),scales=list(draw=FALSE),xlab=NULL, ylab=NULL)

p_hal_down<-levelplot(hal_down,col.regions=sort(terrain.colors(100),decreasing=T),main=expression(italic('H. portulacoides')),margin=FALSE,at=seq(0,1,0.1),scales=list(draw=FALSE),xlab=NULL, ylab=NULL) 

p_ph_down<-levelplot(ph_down,col.regions=sort(terrain.colors(100),decreasing=T),main=expression(italic('P. australis')),margin=FALSE,at=seq(0,1,0.1))

      p_ph_down2 <- spplot(ph_down
		#,scales=list(draw=TRUE)
		#,par.settings=viridisTheme()
		,col.regions=sort(terrain.colors(100),decreasing=T)
		,at=seq(0,1,0.1)
                ,margin=FALSE
		,xlab=NULL, ylab=NULL
		,main=expression(italic('P. australis'))
		,sp.layout=list(arrow,scale,text)
		)

p_mud_down<-levelplot(mud_down
	,col.regions=sort(terrain.colors(100),decreasing=T)
	#,par.settings=viridisTheme()
	,xlab=NULL, ylab=NULL
	,main='Mud flats'
	,margin=FALSE
	,at=seq(0,1,0.1)
	)


# up

ph_up<-raster('phragmites_up.tif')
bol_up<-raster('bolboschoenus_up.tif')
jun_up<-raster('juncus_up.tif')
mud_up<-raster('mud_up.tif')

scaleup = list("SpatialPolygonsRescale", layout.scale.bar(), 
    offset = c(-8.64,40.745), scale = 0.044988708, fill=c("transparent","black"))
textup = list("sp.text", c(-8.62,40.754), "5 km")

arrowup = list("SpatialPolygonsRescale", layout.north.arrow(), 
    offset = c(-8.55,40.735), scale = 0.02)

p_bol_up<-levelplot(bol_up,col.regions=sort(terrain.colors(100),decreasing=T),main=expression(italic('B. maritimus')),margin=FALSE,at=seq(0,1,0.1),scales=list(draw=FALSE),xlab=NULL, ylab=NULL) # viridisTheme()

p_jun_up<-levelplot(jun_up,col.regions=sort(terrain.colors(100),decreasing=T),main=expression(italic('J. maritimus')),margin=FALSE,at=seq(0,1,0.1),scales=list(draw=FALSE),xlab=NULL, ylab=NULL)

p_ph_up<-levelplot(ph_up,col.regions=sort(terrain.colors(100),decreasing=T),main=expression(italic('P. australis')),margin=FALSE,at=seq(0,1,0.1))

      p_ph_up2 <- spplot(ph_up
		#,scales=list(draw=TRUE)
		#,par.settings=viridisTheme()
		,col.regions=sort(terrain.colors(100),decreasing=T)
		,at=seq(0,1,0.1)
                ,margin=FALSE
		,xlab=NULL, ylab=NULL
		,main=expression(italic('P. australis'))
		,sp.layout=list(arrowup,scaleup,textup)
		)

p_mud_up<-levelplot(mud_up,col.regions=sort(terrain.colors(100),decreasing=T),main='Mud flats',margin=FALSE,at=seq(0,1,0.1),scales=list(draw=FALSE),xlab=NULL, ylab=NULL)

# Export
png('fig3.png',width=1152,height=864,res=150) 
grid.arrange(p_ph_down2,p_ph_up2,p_bol_down,p_bol_up,p_jun_down,p_jun_up,p_hal_down,ncol=3)
dev.off()

tiff('fig3.tif',width=1152,height=864,res=150) 
grid.arrange(p_ph_down2,p_ph_up2,p_bol_down,p_bol_up,p_jun_down,p_jun_up,p_hal_down,ncol=3)
dev.off()



