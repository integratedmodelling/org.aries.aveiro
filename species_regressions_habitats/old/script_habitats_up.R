# classification
library(vegan)
library(ggplot2)
library(labdsv)
library(rasterVis)
#library(randomcoloR)
library(RColorBrewer)

source('script_glm_up.R',echo=T)
source('script_predict_up.R',echo=T)

dev.off()

spsum<-sum(sp)

df_sp<-as.data.frame(sp)
indna<-apply(df_sp,1,function(x) unique(as.vector(!is.na(x))))
df_sp2<-df_sp[indna,]

vdist<-vegdist(df_sp2)
hcl<-hclust(vdist, "ward.D2")
classes<-cutree(hcl,k=6)

sps_indval<-indval(df_sp2,classes)
summary(sps_indval)

sink('indval_up.txt')
summary(sps_indval)
sps_indval
sink()

#km6<-kmeans(vdist,6)
#mds<-metaMDS(vdist)

png('hclust_classes_up.png')
plot(hcl,hang=-1)
rect.hclust(hcl,k=6)
dev.off()

r<-sp$Phragmites
valr<-getValues(r)
valr[indna]<-classes
coms<-setValues(r,valr)

table(classes)
#classes
#   1    2    3    4    5    6
#3078 7011 7117 2292 2160 1411

#comscats<-c('Class 1','Class 2','Class 3','Class 4','Class 5','Class 6')

# using Mud & Ward.D2
comscats<-c('Bolboshoenus & Juncus', # 1
	'Bolboschoenus', # 2
	'Bolboschoenus & Phragmites', # 3
	'Juncus', # 4 It could be added to 6
	'Phragmites', # 5
	'Juncus & Phragmites')  # 6

# Not using Mud
#comscats<-c('x', # 1
#	'x & x', # 2
#	'x & x', # 3
#	'x & x & x', # 4
#	'x & x & x)', # 5
#	'x') # 6 

coms<-ratify(coms)
ratcoms <- levels(coms)[[1]]
ratcoms$legend <- comscats
levels(coms) <- ratcoms

pdf('habitat_models_up_betareg_2015.pdf')
levelplot(coms, col.regions=brewer.pal(6,'Set1'), xlab='',ylab='',main=list(label='Donwstream (2015)',cex=1.5),colorkey=list(labels=list(cex=1)))
dev.off()

# Optional

mds_xy <- data.frame(mds$points)
mds_xy$cluster<-as.vector(classes)

png('nmds_classes.png')
ggplot(mds_xy, aes(MDS1, MDS2, color = as.factor(cluster),label = rownames(mds_xy))) + geom_point() + theme_bw() + geom_label()
dev.off()


## downstream (6 habitats)
## upstream (6 habitats)

#A2.53C<- sp$Phragmites > 0.85

#A2.535<- sp$Juncus > 0.65

##mud<- sp$Phragmites < 0.77 & sp$mud > 0.48 

#A2.53Cd<- sp$Phragmites > 0.65 & sp$Halimione > 0.65 & sp$Bolboschoenus > 0.65

#A2.535A2.53C<- sp$Phragmites > 0.46 & sp$Juncus > 0.86 

#A2.5d<-  sp$Halimione > 0.4 & sp$Bolboschoenus > 0.47 

#A2.535d<- sp$Juncus > 0.86 & sp$Halimione < 0.4 & sp$Bolboschoenus < 0.47 

#A2.53Cu<- sp$Phragmites > 0.46 & sp$Bolboschoenus > 0.47 

#A2.535u<- sp$Juncus > 0.86 & sp$Bolboschoenus > 0.47 

##A2.5<- sp$Halimione > 0.72 

#A2.5u<- sp$Bolboschoenus > 0.47


#sumhabs<- A2.53C + A2.535 + mud + A2.53Cd + A2.535A2.53C + A2.5d + A2.535d + A2.53Cu + A2.535u + A2.5 + A2.5u

#hs<-stack(A2.53C,A2.535,mud,A2.53Cd,A2.535A2.53C,A2.5d,A2.535d,A2.53Cu,A2.535u,A2.5,A2.5u)

#names(hs)<-c('A2.53C','A2.535','mud','A2.53Cd','A2.535A2.53C','A2.5d','A2.535d','A2.53Cu','A2.535u','A2.5','A2.5u')

#pdf('habitat_models_down_2015.pdf')
#levelplot(hs,par.settings=YlOrRdTheme())
#dev.off()
