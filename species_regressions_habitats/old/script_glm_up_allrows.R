library(betareg)
library(glmulti)

logistic <- function(p) log(p / (1-p) +0.01)

invlog<-function(p) exp(-1 * p) / ( 1 + exp(-1 * p) )

glm.qbinom <- function (formula, data, always="", ...) {
	glm(as.formula(paste(deparse(formula), always)), family = quasibinomial(link = "logit"), data = data,...)
}

betar <- function (formula, data, always="", ...) {
	betareg(as.formula(paste(deparse(formula), always)), data = data, link = "loglog",...)
}

upstream<-read.table('upstream_allrows.csv',sep=';',header=T)

df_bol<-upstream[upstream$variable=='Bolboschoenus_maritimus',]
df_ph<-upstream[upstream$variable=='Phragmites_australis',]
df_hal<-upstream[upstream$variable=='Halimione_portulacoides',]
df_mud<-upstream[upstream$variable=='mud',]
df_jun<-upstream[upstream$variable=='Juncus_maritimus',]

#df_bol<-upstream[!is.na(upstream$Bolboschoenus_maritimus),1:4]
#df_ph<-upstream[!is.na(upstream$Phragmites_australis),c(1:3,8)]
#df_hal<-upstream[!is.na(upstream$Halimione_portulacoides),c(1:3,5)]
#df_mud<-upstream[!is.na(upstream$mud),c(1:3,7)]
#df_jun<-upstream[!is.na(upstream$Juncus_maritimus),c(1:3,6)]

y.transf.betareg <- function(y){
    n.obs <- sum(!is.na(y))
    (y * (n.obs - 1) + 0.5) / n.obs
}

#opt_bol_d_br<-glmulti(y.transf.betareg(value1) ~ dist_watercourse + sal_mean + elev, data = df_bol,fitfunction=betar)

#ind<-logistic(df_bol_d$value1)<Inf
#opt_bol_d_log<-glmulti(logistic(value1) ~ dist_watercourse + sal_mean + elev, data = df_bol_d[ind,])

#bol_d_qb<-glm(value1 ~ sal_mean + sal_mean:dist_watercourse,data=df_bol_d,family=quasibinomial(link = "logit"))

#opt_hal_d_br<-glmulti(y.transf.betareg(value1) ~ dist_watercourse + sal_mean + elev, data = df_hal_d,fitfunction=betar)

#opt_ph_d_br<-glmulti(y.transf.betareg(value1) ~ dist_watercourse + sal_mean + elev, data = df_ph_d,fitfunction=betar)

opt_ph_down_br<-glmulti(y.transf.betareg(value1) ~ dist_watercourse + sal_mean + elev, data = df_ph,fitfunction=betar)

opt_jun_down_br<-glmulti(y.transf.betareg(value1) ~ dist_watercourse + sal_mean + elev, data = df_jun,fitfunction=betar)

opt_bol_down_br<-glmulti(y.transf.betareg(value1) ~ dist_watercourse + sal_mean + elev, data = df_bol,fitfunction=betar)

opt_hal_down_br<-glmulti(y.transf.betareg(value1) ~ dist_watercourse + sal_mean + elev, data = df_hal,fitfunction=betar)

opt_mud_down_br<-glmulti(y.transf.betareg(value1) ~ dist_watercourse + sal_mean + elev, data = df_mud,fitfunction=betar)

#opt_mud_d_br<-glmulti(y.transf.betareg(value1) ~ dist_watercourse + sal_mean + elev, data = df_mud_d,fitfunction=betar)

#opt_jun_d_br<-glmulti(y.transf.betareg(value1) ~ dist_watercourse + sal_mean + elev, data = df_jun_d,fitfunction=betar)

#library(MuMin)
#options(na.action = "na.fail")
#dd<-dredge(bol_d_qb)
#summary(get.models(dd, 1)[[1]])
#summary(get.models(dd, 2)[[1]])

#library(bestglm)
#b<-bestglm(df_bol_d[,c(6:7,9:10,13)])

#library(randomForest)

#rfbol<-randomForest(df_bol_d[,c(7,9:10)],df_bol_d[,13],proximity=T)

#rfjun<-randomForest(df_jun_d[,c(7,9:10)],df_jun_d[,13],proximity=T)

#rfhal<-randomForest(df_hal_d[,c(7,9:10)],df_hal_d[,13],proximity=T)

#rfmud<-randomForest(df_mud_d[,c(7,9:10)],df_mud_d[,13],proximity=T)

#rfph<-randomForest(df_ph_d[,c(7,9:10)],df_ph_d[,13],proximity=T)


