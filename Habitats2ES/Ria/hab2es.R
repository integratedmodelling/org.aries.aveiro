library(raster)

ria_patch<-raster('habs_ria_hid.tif')

# Ria
ria_ESS_C_SpiritSymb_SpiritualEmblematic<-ria_patch
ria_ESS_P_En_BiomassBasedEnergySources<-ria_patch
ria_ABO_P_NutAbSubst_Mineral<-ria_patch

ria_ABO_C_PhysIntel_IntellectualRepresentativeInteractions<-ria_patch
ria_ABO_C_PhysIntel_PhysicalExperientialInteractions<-ria_patch
ria_ABO_P_AbMat_NonMetallic<-ria_patch
ria_ABO_P_AbMat_Water<-ria_patch
ria_ABO_P_NutAbSubst_Water<-ria_patch
ria_ABO_RM_MedFlo_BySolidLiquidGaseousFlows<-ria_patch
ria_ABO_RM_MedWast_ByNaturalChemicalPhysicalProcesses<-ria_patch
ria_ESS_C_PhysIntel_IntellectualRepresentativeInteractions<-ria_patch
ria_ESS_C_PhysIntel_PhysicalExperientialInteractions<-ria_patch
ria_ESS_C_SpiritSymb_OtherCulturalOutputs<-ria_patch
ria_ESS_P_En_MechanicalEnergy<-ria_patch
ria_ESS_P_Mat_Biomass<-ria_patch
ria_ESS_P_Nut_Biomass<-ria_patch
ria_ESS_RM_MaintPhChBioCond_AtmosphericCompositionClimateRegulation<-ria_patch
ria_ESS_RM_MaintPhChBioCond_LifecycleMaintHabitatGenePoolProtection<-ria_patch
ria_ESS_RM_MaintPhChBioCond_PestDiseaseControl<-ria_patch
ria_ESS_RM_MaintPhChBioCond_SoilFormationComposition<-ria_patch
ria_ESS_RM_MaintPhChBioCond_WaterConditions<-ria_patch
ria_ESS_RM_MedFlo_GaseousAirFlows<-ria_patch
ria_ESS_RM_MedFlo_LiquidFlows<-ria_patch
ria_ESS_RM_MedFlo_MassFlows<-ria_patch
ria_ESS_RM_MedWast_MediationBiota<-ria_patch
ria_ESS_RM_MedWast_MediationEcosystems<-ria_patch

bvlcs5<-read.table('ria.csv',sep=';',header=T)

for(h in 6:dim(bvlcs5)[2]){ # col
	for(n in 1:dim(bvlcs5)[1]){ # row

eval(parse(text=paste('ria_',names(bvlcs5)[h],'[ria_patch==bvlcs5[',n,',4]]<-bvlcs5[',n,',',h,']',sep='')))



}

eval(parse(text=paste("writeRaster(ria_",names(bvlcs5)[h],",'ES/ria_",names(bvlcs5)[h],".tif',overwrite=T)",sep="")))

}




