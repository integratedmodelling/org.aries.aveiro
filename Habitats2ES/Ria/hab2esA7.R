library(raster)

riaA7_patch<-raster('habA7_ria_hid.tif')

# Ria
riaA7_ESS_C_SpiritSymb_SpiritualEmblematic<-riaA7_patch
riaA7_ESS_P_En_BiomassBasedEnergySources<-riaA7_patch
riaA7_ABO_P_NutAbSubst_Mineral<-riaA7_patch

riaA7_ABO_C_PhysIntel_IntellectualRepresentativeInteractions<-riaA7_patch
riaA7_ABO_C_PhysIntel_PhysicalExperientialInteractions<-riaA7_patch
riaA7_ABO_P_AbMat_NonMetallic<-riaA7_patch
riaA7_ABO_P_AbMat_Water<-riaA7_patch
riaA7_ABO_P_NutAbSubst_Water<-riaA7_patch
riaA7_ABO_RM_MedFlo_BySolidLiquidGaseousFlows<-riaA7_patch
riaA7_ABO_RM_MedWast_ByNaturalChemicalPhysicalProcesses<-riaA7_patch
riaA7_ESS_C_PhysIntel_IntellectualRepresentativeInteractions<-riaA7_patch
riaA7_ESS_C_PhysIntel_PhysicalExperientialInteractions<-riaA7_patch
riaA7_ESS_C_SpiritSymb_OtherCulturalOutputs<-riaA7_patch
riaA7_ESS_P_En_MechanicalEnergy<-riaA7_patch
riaA7_ESS_P_Mat_Biomass<-riaA7_patch
riaA7_ESS_P_Nut_Biomass<-riaA7_patch
riaA7_ESS_RM_MaintPhChBioCond_AtmosphericCompositionClimateRegulation<-riaA7_patch
riaA7_ESS_RM_MaintPhChBioCond_LifecycleMaintHabitatGenePoolProtection<-riaA7_patch
riaA7_ESS_RM_MaintPhChBioCond_PestDiseaseControl<-riaA7_patch
riaA7_ESS_RM_MaintPhChBioCond_SoilFormationComposition<-riaA7_patch
riaA7_ESS_RM_MaintPhChBioCond_WaterConditions<-riaA7_patch
riaA7_ESS_RM_MedFlo_GaseousAirFlows<-riaA7_patch
riaA7_ESS_RM_MedFlo_LiquidFlows<-riaA7_patch
riaA7_ESS_RM_MedFlo_MassFlows<-riaA7_patch
riaA7_ESS_RM_MedWast_MediationBiota<-riaA7_patch
riaA7_ESS_RM_MedWast_MediationEcosystems<-riaA7_patch

bvlcs5<-read.table('ria.csv',sep=';',header=T)

for(h in 6:dim(bvlcs5)[2]){ # col
	for(n in 1:dim(bvlcs5)[1]){ # row

eval(parse(text=paste('riaA7_',names(bvlcs5)[h],'[riaA7_patch==bvlcs5[',n,',4]]<-bvlcs5[',n,',',h,']',sep='')))



}

eval(parse(text=paste("writeRaster(riaA7_",names(bvlcs5)[h],",'ES/riaA7_",names(bvlcs5)[h],".tif',overwrite=T)",sep="")))

}




