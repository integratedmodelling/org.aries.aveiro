library(rgdal)
library(sp)
library(raster)

hid<-read.table('ria_hid.csv',sep=';',header=T)

habs_ria<-readOGR(dsn='.',lay='RiadeAveiro_Habitats')

missing<-!is.element(hid$EUNIS_CODE,unique(habs_ria@data$EUNIS_CODE))

levels(habs_ria@data$EUNIS_CODE)<-c(levels(habs_ria@data$EUNIS_CODE),paste(hid$EUNIS_CODE[missing]))

#unique(habs_ria@data$EUNIS_CODE)

#levels(habs_ria@data$EUNIS_CODE)<-c(levels(habs_ria@data$EUNIS_CODE),'I1.5')
habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_CODE=='I1.53']<-'I1.5'

habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_CODE=='A2.2' & habs_ria@data$Realm=='Inlets and Transitional']<-'A2.2i'

habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_CODE=='A2.2' & habs_ria@data$Realm=='Coastal']<-'A2.2c'

habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_3=='A2.6']<-'A2.6'

habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_CODE=='A5.2' & habs_ria@data$Realm=='Inlets and Transitional']<-'A5.2i'

habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_CODE=='A5.2' & habs_ria@data$Realm=='Coastal']<-'A5.2c'

habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_CODE=='A5.2' & habs_ria@data$Realm=='Shelf']<-'A5.2s'

habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_3=='A5.3']<-'A5.3'
habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_3=='A5.4']<-'A5.4'
habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_3=='C3.2']<-'C3.2'
habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_3=='E5.4']<-'E5.4'
habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_3=='G1.2']<-'G1.2'
habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_3=='G1.3']<-'G1.3'
habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_3=='J5.1']<-'J5.1'

# categories in the shapefile that do not match the ES look-up table

sonstige<-!is.element(habs_ria@data$EUNIS_CODE,unique(hid$EUNIS_CODE))

unique(habs_ria@data$EUNIS_CODE[sonstige])
# A5.23
habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_CODE=='A5.23']<-'A5.2c'

#A5.24*
habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_CODE=='A5.24*']<-'A5.2c'

#A5.25
habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_CODE=='A5.25']<-'A5.2c'

#A5.25*
habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_CODE=='A5.25*']<-'A5.2s'

#A5.22
habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_CODE=='A5.22']<-'A5.2i'

#A5.23*
habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_CODE=='A5.23*']<-'A5.2s'

#A5.24
habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_CODE=='A5.24']<-'A5.2i'

# A2.53C
habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_CODE=='A2.53C']<-'A2.5'

#A2.554
habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_CODE=='A2.554']<-'A2.5'

#A2.535
habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_CODE=='A2.535']<-'A2.5'

# A2.22
habs_ria@data$EUNIS_CODE[habs_ria@data$EUNIS_CODE=='A2.22' & habs_ria@data$Realm=='Coastal']<-'A2.2c'


# look for string in whole data frame
# for(n in 1:dim(habs_ria@data)[2]){print(table(habs_ria@data[,n]=='J5.1'))}



#classes<-c('I1.1','I1.4','I1.5','X10','A2.3','A2.551','A2.554','A5.2','A5.3','G1.1','C2.3')

#habs_bvl_patch<-subset(habs_bvl, is.element(Class,classes))

habs_ria_hid<-merge(habs_ria,hid,by='EUNIS_CODE')


writeOGR(habs_ria_hid,dsn='.','RiadeAveiro_Habitats_reclass_hid',driver="ESRI Shapefile",overwrite_layer=T)


## rasterize
#ria<-raster('cienc.tif')

ndv=-32767
tmp.raster <- raster()
extent(tmp.raster) <- extent(habs_ria_hid)
res(tmp.raster) <- 0.0003# res(ria) # aprox. 0.001
r_habs_ria_hid <- rasterize(habs_ria_hid, tmp.raster,field='hid')
r_habs_ria_hid@file@nodatavalue<-ndv
writeRaster(r_habs_ria_hid,'habs_ria_hid.tif', overwrite=T)

# Reclassify sdm based habitat rasters

#sdm_down_reclass<-reclassify(sdm_down,
#	c(
#	3,3,8,
#	4,4,9,
#	2,2,14,
#	1,1,11,
#	5,5,12
#))

#sdm_down_reclass<-sdm_down

#sdm_down_reclass[sdm_down_reclass==3]<-8
#sdm_down_reclass[sdm_down_reclass==4]<-9
#sdm_down_reclass[sdm_down_reclass==2]<-14
#sdm_down_reclass[sdm_down_reclass==1]<-11
#sdm_down_reclass[sdm_down_reclass==5]<-12

#writeRaster(sdm_down_reclass,'sdm_down_reclass.tif', overwrite=T)

## Downstream
## id	hid
##6	6	A2.5*d
##3	8	A2.535
##4	9	A2.535*d
##2	14	A2.535/A2.53C
##1	11	A2.53C
##5	12	A2.53C*d

#sdm_up_reclass<-sdm_up

#sdm_up_reclass[sdm_up_reclass==6]<-7
#sdm_up_reclass[sdm_up_reclass==5]<-8
#sdm_up_reclass[sdm_up_reclass==2]<-11
#sdm_up_reclass[sdm_up_reclass==3]<-10
#sdm_up_reclass[sdm_up_reclass==4]<-13
#sdm_up_reclass[sdm_up_reclass==1]<-14

#writeRaster(sdm_up_reclass,'sdm_up_reclass.tif', overwrite=T)

# Upstream
# id	hid
#6	7	A2.5*u
#5	8	A2.535
#3	10	A2.535*u
#1	14	A2.535/A2.53C
#2	11	A2.53C
#4	13	A2.53C*u


