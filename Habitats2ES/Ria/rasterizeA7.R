library(rgdal)
library(sp)
library(raster)

hid<-read.table('ria_hid.csv',sep=';',header=T)

habs_ria<-readOGR(dsn='.',lay='RiadeAveiro_HabitatA7_wgs84')

levels(habs_ria@data$EUNIS_CODE)<-c(levels(habs_ria@data$EUNIS_CODE),c('A7c','A7s','A7i'))

habs_ria@data$EUNIS_CODE[habs_ria@data$Realm=='Inlets and Transitional']<-'A7i'
habs_ria@data$EUNIS_CODE[habs_ria@data$Realm=='Coastal']<-'A7c'
habs_ria@data$EUNIS_CODE[habs_ria@data$Realm=='Shelf']<-'A7s'

habs_ria_hid<-merge(habs_ria,hid,by='EUNIS_CODE')

writeOGR(habs_ria_hid,dsn='.','RiadeAveiro_HabitatA7_reclass_hid',driver="ESRI Shapefile",overwrite_layer=T)


## rasterize

ria<-raster('habs_ria_hid.tif')

ndv=-32767
tmp.raster <- raster()
extent(tmp.raster) <- extent(ria)#extent(habs_ria_hid)
res(tmp.raster) <- res(ria)#0.0003
r_habs_ria_hid <- rasterize(habs_ria_hid, tmp.raster,field='hid')
r_habs_ria_hid@file@nodatavalue<-ndv
writeRaster(r_habs_ria_hid,'habA7_ria_hid.tif', overwrite=T)

