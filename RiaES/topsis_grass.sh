r.in.gdal -oe in=final_ria_s1.tif out=final_ria_s1
r.in.gdal -oe in=final_ria_s3.tif out=final_ria_s3
r.in.gdal -oe in=final_ria_s4.tif out=final_ria_s4
r.in.gdal -oe in=final_ria_s5.tif out=final_ria_s5
r.in.gdal -oe in=final_ria_s6.tif out=final_ria_s6
r.in.gdal -oe in=final_ria_s7.tif out=final_ria_s7
r.in.gdal -oe in=final_ria_s8.tif out=final_ria_s8
r.in.gdal -oe in=final_ria_s9.tif out=final_ria_s9
r.in.gdal -oe in=final_ria_s10.tif out=final_ria_s10
r.in.gdal -oe in=final_ria_s11.tif out=final_ria_s11

# compromise

#R
library(scales)
w0<-c(4.6,3.9,3.7,5.5,3.5,4,6.3,8.2,5.1,5.7)
w1<-rescale(w0,to=c(0,1))
#R

r.mcda.topsis criteria=final_ria_s1,final_ria_s3,final_ria_s4,final_ria_s5,final_ria_s6,final_ria_s7,final_ria_s8,final_ria_s9,final_ria_s10,final_ria_s11 weights=0.23404255,0.08510638,0.04255319,0.42553191,0.00000000,0.10638298,0.59574468,1.00000000,0.34042553,0.46808511 preferences=gain,gain,gain,gain,gain,gain,gain,gain,gain,gain topsismap=smca_compromise_ria

r.out.gdal in=smca_compromise_ria out=ria_smca_compromise_topsis.tif --o

# group1

#R
library(scales)
w0<-c(4,4.1,5.9,1,6,1.4,4.2,7.1,8,10)
w1<-rescale(w0,to=c(0,1))
#R

r.mcda.topsis criteria=final_ria_s1,final_ria_s3,final_ria_s4,final_ria_s5,final_ria_s6,final_ria_s7,final_ria_s8,final_ria_s9,final_ria_s10,final_ria_s11 weights=0.33333333,0.34444444,0.54444444,0.00000000,0.55555556,0.04444444,0.35555556,0.67777778,0.77777778,1.00000000 preferences=gain,gain,gain,gain,gain,gain,gain,gain,gain,gain topsismap=smca_group1_ria

r.out.gdal in=smca_compromise_ria out=ria_smca_group1_topsis.tif --o
