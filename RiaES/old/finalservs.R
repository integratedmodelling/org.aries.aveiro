library(raster)

ABOCPhysIntelIntellectualRepresentativeInteractions<-raster('ABOCPhysIntelIntellectualRepresentativeInteractions.tif')
ABOCPhysIntelPhysicalExperientialInteractions<-raster('ABOCPhysIntelPhysicalExperientialInteractions.tif')
ABORMMedFloBySolidLiquidGaseousFlows<-raster('ABORMMedFloBySolidLiquidGaseousFlows.tif')
ABORMMedWastByNaturalChemicalPhysicalProcesses<-raster('ABORMMedWastByNaturalChemicalPhysicalProcesses.tif')
ESSCPhysIntelIntellectualRepresentativeInteractions<-raster('ESSCPhysIntelIntellectualRepresentativeInteractions.tif')
ESSCPhysIntelPhysicalExperientialInteractions<-raster('ESSCPhysIntelPhysicalExperientialInteractions.tif')
ESSCSpiritSymbOtherCulturalOutputs<-raster('ESSCSpiritSymbOtherCulturalOutputs.tif')
ESSCSpiritSymbSpiritualEmblematic<-raster('ESSCSpiritSymbSpiritualEmblematic.tif')
ESSPEnBiomassBasedEnergySources<-raster('ESSPEnBiomassBasedEnergySources.tif')
ESSPEnMechanicalEnergy<-raster('ESSPEnMechanicalEnergy.tif')
ESSRMMaintPhChBioCondAtmosphericCompositionClimateRegulation<-raster('ESSRMMaintPhChBioCondAtmosphericCompositionClimateRegulation.tif')
ESSRMMaintPhChBioCondLifecycleMaintHabitatGenePoolProtection<-raster('ESSRMMaintPhChBioCondLifecycleMaintHabitatGenePoolProtection.tif')
ESSRMMaintPhChBioCondPestDiseaseControl<-raster('ESSRMMaintPhChBioCondPestDiseaseControl.tif')
ESSRMMaintPhChBioCondSoilFormationComposition<-raster('ESSRMMaintPhChBioCondSoilFormationComposition.tif')
ESSRMMaintPhChBioCondWaterConditions<-raster('ESSRMMaintPhChBioCondWaterConditions.tif')
ESSRMMedFloGaseousAirFlows<-raster('ESSRMMedFloGaseousAirFlows.tif')
ESSRMMedFloLiquidFlows<-raster('ESSRMMedFloLiquidFlows.tif')
ESSRMMedFloMassFlows<-raster('ESSRMMedFloMassFlows.tif')
ESSRMMedWastMediationBiota<-raster('ESSRMMedWastMediationBiota.tif')
ESSRMMedWastMediationEcosystems<-raster('ESSRMMedWastMediationEcosystems.tif')


s1<-ESSPEnBiomassBasedEnergySources + ESSPEnMechanicalEnergy

#s3<-ESSPMatBiomass

#s4<-raster(ABOPAbMatWater)

#s5<-ESSPNutBiomass

#s6<-ABOPNutAbSubstWater

s7<-ESSRMMedFloGaseousAirFlows + ESSRMMedFloLiquidFlows + ESSRMMedFloMassFlows + ABORMMedFloBySolidLiquidGaseousFlows

s8<-ABORMMedWastByNaturalChemicalPhysicalProcesses + ESSRMMedWastMediationBiota + ESSRMMedWastMediationEcosystems

s9<-ESSRMMaintPhChBioCondAtmosphericCompositionClimateRegulation + ESSRMMaintPhChBioCondLifecycleMaintHabitatGenePoolProtection + ESSRMMaintPhChBioCondPestDiseaseControl + ESSRMMaintPhChBioCondSoilFormationComposition + ESSRMMaintPhChBioCondWaterConditions

s10<-ABOCPhysIntelIntellectualRepresentativeInteractions + ABOCPhysIntelPhysicalExperientialInteractions + ESSCPhysIntelIntellectualRepresentativeInteractions + ESSCPhysIntelPhysicalExperientialInteractions

s11<-ESSCSpiritSymbOtherCulturalOutputs + ESSCSpiritSymbSpiritualEmblematic

writeRaster(s1,'s1.tif')
writeRaster(s7,'s7.tif')
writeRaster(s8,'s8.tif')
writeRaster(s9,'s9.tif')
writeRaster(s10,'s10.tif')
writeRaster(s11,'s11.tif')


