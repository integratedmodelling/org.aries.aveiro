library(lubridate)
library(plyr)

data<-read.table('serie_12102016100621.csv',sep=',',header=T)
dmy_hm(data[,1])->data[,5]
date(data[,5])->data[,6]

ddply(data,~V6,summarise,min=min(X08G01C))->tmin_08G01C_day
ddply(data,~V6,summarise,max=max(X08G01C))->tmax_08G01C_day
ddply(data,~V6,summarise,min=min(X11E01C))->tmin_11E01C_day
ddply(data,~V6,summarise,max=max(X11E01C))->tmax_11E01C_day
ddply(data,~V6,summarise,min=min(X09I01C))->tmin_09I01C_day
ddply(data,~V6,summarise,max=max(X09I01C))->tmax_09I01C_day

merge(tmin_08G01C_day,tmax_08G01C_day,by='V6')->temp_08G01C_day
merge(tmin_11E01C_day,tmax_11E01C_day,by='V6')->temp_11E01C_day
merge(tmin_09I01C_day,tmax_09I01C_day,by='V6')->temp_09I01C_day

#write.table(temp_08G01C_day,'temp_08G01C_day.csv',row.names=F,sep=',',na='')
#write.table(temp_11E01C_day,'temp_11E01C_day.csv',row.names=F,sep=',',na='')
#write.table(temp_09I01C_day,'temp_09I01C_day.csv',row.names=F,sep=',',na='')

dataprec<-read.table('PrecipitationData_3stations.csv',sep=',',header=T)
dmy_hm(dataprec[,1])->dataprec[,5]
date(dataprec[,5])->dataprec[,6]

merge(temp_08G01C_day,dataprec[,c(2,6)],by='V6')->temp_prec_08G01C_day
merge(temp_09I01C_day,dataprec[,c(4,6)],by='V6')->temp_prec_09I01C_day
merge(temp_11E01C_day,dataprec[,c(3,6)],by='V6')->temp_prec_11E01C_day

write.table(temp_prec_08G01C_day,'temp_prec_08G01C_day.csv',row.names=F,sep=',',na='')
write.table(temp_prec_11E01C_day,'temp_prec_11E01C_day.csv',row.names=F,sep=',',na='')
write.table(temp_prec_09I01C_day,'temp_prec_09I01C_day.csv',row.names=F,sep=',',na='')


